<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220328135515 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE notification CHANGE subject subject MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE theme CHANGE description description MEDIUMTEXT NOT NULL');
        $this->addSql('ALTER TABLE vote CHANGE voted_full_agreement voted_full_agreement TINYINT(1) NOT NULL, CHANGE voted_agree voted_agree TINYINT(1) NOT NULL, CHANGE voted_mixed voted_mixed TINYINT(1) NOT NULL, CHANGE voted_disagree voted_disagree TINYINT(1) NOT NULL, CHANGE voted_total_disagreement voted_total_disagreement TINYINT(1) NOT NULL');
        $this->addSql('ALTER TABLE website ADD mail_registration LONGTEXT DEFAULT NULL, CHANGE title title VARCHAR(40) NOT NULL, CHANGE version version VARCHAR(40) NOT NULL, CHANGE name name VARCHAR(40) NOT NULL, CHANGE email email VARCHAR(40) NOT NULL, CHANGE background_color background_color VARCHAR(40) DEFAULT NULL');
        $this->addSql('ALTER TABLE workshop CHANGE description description MEDIUMTEXT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE notification CHANGE subject subject MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE theme CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE vote CHANGE voted_full_agreement voted_full_agreement TINYINT(1) DEFAULT NULL, CHANGE voted_agree voted_agree TINYINT(1) DEFAULT NULL, CHANGE voted_mixed voted_mixed TINYINT(1) DEFAULT NULL, CHANGE voted_disagree voted_disagree TINYINT(1) DEFAULT NULL, CHANGE voted_total_disagreement voted_total_disagreement TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE website DROP mail_registration, CHANGE title title VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE version version VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE name name VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE email email VARCHAR(255) CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`, CHANGE background_color background_color VARCHAR(255) CHARACTER SET utf8mb4 DEFAULT NULL COLLATE `utf8mb4_unicode_ci`');
        $this->addSql('ALTER TABLE workshop CHANGE description description MEDIUMTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_unicode_ci`');
    }
}

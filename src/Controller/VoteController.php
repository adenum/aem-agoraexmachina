<?php
namespace App\Controller;

use App\Entity\Vote;
use App\Entity\Proposal;
use App\Entity\User;
use App\Repository\DelegationRepository;
use App\Repository\VoteRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class VoteController Cette classe s'occupe de l'ajout et la suppression des votes
 * @package App\Controller
 */
class VoteController extends AbstractController
{

    /**
     * @Route("/{slug}/workshop/proposal/{proposal}/vote/{userVote}/user/{user}", name="vote_add", methods={"GET", "POST"})
     * @param string $slug partie de l'URL de la page, composé du thème et de l'atelier
     * @param Proposal $proposal La proposition sur laquelle le vote se fait
     * @param string $userVote La string envoyé par notre template selon le clic de l'utilisateur. Utilisé pour connaitre la valeur du vote
     * @param User $user L'utilisateur votant
     * Fonction qui ajoute le vote de l'utilisateur connecté
     */
    public function addVote(string $slug, Proposal $proposal, string $userVote, User $user): Response
    {
        //On vérifie que l'utilisateur fait bien partie de la cohorte liée à la proposition
        if ($proposal->getWorkshop()->getTheme()->getCategory()->getUsers()->contains($this->getUser())) {

            $entityManager = $this->getDoctrine()->getManager();

            $theme = $proposal->getWorkshop()->getTheme();

            //On récupère le vote de l'utilisateur connecté
            $vote = $entityManager->getRepository(Vote::class)->findOneBy([
                    'user' => $user,
                    'proposal' => $proposal
                ]
            );

            //On crée un nouveau Vote uniquement si l'utilisateur n'a pas déjà voté
            if (!$vote)
                $vote = new Vote();

            //Fonction qui traite le vote
            switch ($theme->getVoteType()) {
                case 'weighted' : $this->voteWeighted($vote, $userVote, $proposal, $user, $entityManager); break;
                case 'levelled-3' : $this->voteLevelled3($vote, $userVote, $proposal, $user, $entityManager); break;
                case 'levelled-5' : $this->voteLevelled5($vote, $userVote, $proposal, $user, $entityManager); break;
            }

            //Remplacé par l'AJAX
            //On indique le succès de l'opération et reste sur la même page
            $this->addFlash("success", "vote.success");
            return $this->forward('App\\Controller\\WorkshopController::show', [
                'slug' => $slug,
                'workshop' => $proposal->getWorkshop(),
            ]);
        } else {
            $this->addFlash("error", "vote.error");
            return $this->forward('App\\Controller\\WorkshopController::show', [
                'slug' => $slug,
                'workshop' => $proposal->getWorkshop(),
            ]);
        }
    }

    /**
     * @Route("/{slug}/workshop/proposal/{proposal}/vote/{userVote}/allDelegations", name="vote_add_all_delegations", methods={"GET", "POST"})
     * @param string $slug partie de l'URL de la page, composé du thème et de l'atelier
     * @param Proposal $proposal La proposition sur laquelle le vote se fait
     * @param string $userVote La string envoyé par notre template selon le clic de l'utilisateur. Utilisé pour connaitre la valeur du vote
     * Fonction qui ajoute plusieurs votes d'un coup à une proposition (toutes les délégations reçues par l'utilisateur)
     */
    public function addVoteForAllDelegations(string $slug, Proposal $proposal, string $userVote,
                                             DelegationRepository $delegationRepository): Response
    {
        //On vérifie que l'utilisateur fait bien partie de la cohorte liée à la proposition
        if ($proposal->getWorkshop()->getTheme()->getCategory()->getUsers()->contains($this->getUser())) {

            $entityManager = $this->getDoctrine()->getManager();
            $theme = $proposal->getWorkshop()->getTheme();

            //On récupère toutes les délégations reçues par l'utilisateur dans le thème de la proposition
            $delegations = $delegationRepository->findBy(['userTo' => $this->getUser(), 'theme' => $theme]);

            //Pour chaque délégation, on récupère l'utilisateur en question et son vote
            foreach ($delegations as $delegation) {
                $user = $delegation->getUserFrom();
                $vote = $entityManager->getRepository(Vote::class)->findOneBy([
                        'user' => $user,
                        'proposal' => $proposal
                    ]
                );

                //On crée un nouveau Vote uniquement si cet utilisateur n'a pas déjà voté
                if (!$vote)
                    $vote = new Vote();

                //Fonction qui traite le vote
                switch ($theme->getVoteType()) {
                    case 'weighted' : $this->voteWeighted($vote, $userVote, $proposal, $user, $entityManager); break;
                    case 'levelled-3' : $this->voteLevelled3($vote, $userVote, $proposal, $user, $entityManager); break;
                    case 'levelled-5' : $this->voteLevelled5($vote, $userVote, $proposal, $user, $entityManager); break;
                }
            }

            //On indique le succès de l'opération
            $this->addFlash("success", "vote.success");
        } else {
            $this->addFlash("error", "vote.error");
        }
        return $this->forward('App\\Controller\\WorkshopController::show', [
            'slug' => $slug,
            'workshop' => $proposal->getWorkshop(),
        ]);
    }


    /**
     * @Route("/{slug}/workshop/proposal/{proposal}/vote/delete", name="vote_delete", methods={"GET", "POST"})
     * @param Vote $vote Vote à supprimer
     * @param string $slug Partie de l'URL de la page, composé du thème et de l'atelier
     * Fonction qui supprime le vote de l'utilisateur
     */
    public function delete(Vote $vote, string $slug): Response
    {
        $entityManager = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $proposal = $vote->getProposal();

        //On récupère le vote de l'utilisateur connecté
        $vote = $entityManager->getRepository(Vote::class)->findOneBy([
                'user' => $user,
                'proposal' => $proposal
            ]
        );

        $entityManager->remove($vote);
        $entityManager->flush();

        $this->addFlash("success", "delete.success");

        //Redirection à l'index des propositions
        return $this->redirectToRoute('workshop_show', [
            'slug' => $slug,
            'workshop' => $vote->getProposal()->getWorkshop()->getId(),
        ]);
    }


    /**
     * @Route("/{slug}/workshop/proposal/{proposal}/vote/deleteForAllDelegations", name="vote_delete_all_delegations", methods={"GET", "POST"})
     * @param string $slug Partie de l'URL de la page, composé du thème et de l'atelier
     * Fonction qui supprime tous les votes faits par l'utilisateur via délégation
     */
    public function deleteForAllDelegations(Proposal               $proposal, string $slug,
                                            DelegationRepository   $delegationRepository,
                                            EntityManagerInterface $entityManager,
                                            VoteRepository         $voteRepository): Response
    {
        $workshop = $proposal->getWorkshop();
        $theme = $proposal->getWorkshop()->getTheme();
        $delegations = $delegationRepository->findBy(['userTo' => $this->getUser(), 'theme' => $theme]);

        foreach ($delegations as $delegation) {
            $user = $delegation->getUserFrom();
            $vote = $voteRepository->findOneBy([
                    'user' => $user,
                    'proposal' => $proposal
                ]
            );
            $entityManager->remove($vote);
        }
        $entityManager->flush();

        $this->addFlash("success", "delete.success");

        //Redirection à l'index des propositions
        return $this->redirectToRoute('workshop_show', [
            'slug' => $slug,
            'workshop' => $workshop->getId(),
        ]);
    }


    /**
     * Fonction qui traite le vote à points
     */
    public function voteWeighted($vote, string $userVote, Proposal $proposal, ?user $user, ObjectManager $entityManager): void
    {
        //Vote à points
        //Si le form est validé avec la case vide, on envoi un vote à 0 point
        if ($_POST['selectPoints' . $proposal->getid()] == null)
            $_POST['selectPoints' . $proposal->getid()] = 0;
        $vote->setVotedPoints(($userVote == 'votedPoints') ? $_POST['selectPoints' . $proposal->getid()] : null);

        $this->setAndPersist($vote, $user, $proposal, $entityManager);
    }

    /**
     * Fonction qui traite le vote à trois niveaux
     */
    public function voteLevelled3($vote, string $userVote, Proposal $proposal, ?user $user, ObjectManager $entityManager): void
    {

        //Vote à 3 niveaux
        $vote->setVotedFor(($userVote == "votedFor") ? 1 : 0);
        $vote->setVotedAgainst(($userVote == "votedAgainst") ? 1 : 0);
        $vote->setVotedBlank(($userVote == "votedBlank") ? 1 : 0);

        $this->setAndPersist($vote, $user, $proposal, $entityManager);
    }

    /**
     * Fonction qui traite le vote à cinq niveaux
     */
    public function voteLevelled5($vote, string $userVote, Proposal $proposal, ?user $user, ObjectManager $entityManager): void
    {
        //Vote à 5 niveaux
        $vote->setVotedFullAgreement(($userVote == "votedFullAgreement") ? 1 : 0);
        $vote->setVotedAgree(($userVote == "votedAgree") ? 1 : 0);
        $vote->setVotedMixed(($userVote == "votedMixed") ? 1 : 0);
        $vote->setVotedDisagree(($userVote == "votedDisagree") ? 1 : 0);
        $vote->setVotedTotalDisagreement(($userVote == "votedTotalDisagreement") ? 1 : 0);

        $this->setAndPersist($vote, $user, $proposal, $entityManager);
    }


    public function setAndPersist($vote, ?User $user, Proposal $proposal, ObjectManager $entityManager): void
    {
        //On attribue le vote à l'utilisateur à qui il appartient et à la proposition en cours
        $vote->setUser($user);
        $vote->setProposal($proposal);

        $entityManager->persist($vote);
        $entityManager->flush();
    }

}